class CreateBebeks < ActiveRecord::Migration[5.2]
  def change
    create_table :bebeks do |t|
      t.string :nama
      t.string :email
      t.string :password

      t.timestamps
    end
  end
end
