class CreateDiskusis < ActiveRecord::Migration[5.2]
  def change
    create_table :diskusis do |t|
      t.text :isi
      t.date :tgl
      t.references :orangs, foreign_key: true

      t.timestamps
    end
  end
end
