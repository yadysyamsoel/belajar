class CreateCommentdis < ActiveRecord::Migration[5.2]
  def change
    create_table :commentdis do |t|
      t.string :commenter
      t.string :isi
      t.references :diskusi, foreign_key: true

      t.timestamps
    end
  end
end
