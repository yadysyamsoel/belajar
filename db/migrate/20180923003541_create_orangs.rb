class CreateOrangs < ActiveRecord::Migration[5.2]
  def change
    create_table :orangs do |t|
      t.string :nama
      t.string :alamat
      t.string :jk
      t.string :foto

      t.timestamps
    end
  end
end
