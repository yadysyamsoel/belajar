class CreateKomentars < ActiveRecord::Migration[5.2]
  def change
    create_table :komentars do |t|
      t.string :commet
      t.string :isi
      t.references :disku, foreign_key: true

      t.timestamps
    end
  end
end
