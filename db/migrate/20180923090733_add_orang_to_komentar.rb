class AddOrangToKomentar < ActiveRecord::Migration[5.2]
  def change
    add_reference :komentars, :orang, foreign_key: true
  end
end
