class CreateBls < ActiveRecord::Migration[5.2]
  def change
    create_table :bls do |t|
      t.text :isi
      t.string :org
      t.references :pesan, foreign_key: true

      t.timestamps
    end
  end
end
