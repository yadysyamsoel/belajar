class AddAttachmentImageToOrangs < ActiveRecord::Migration[5.2]
  def self.up
    change_table :orangs do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :orangs, :image
  end
end
