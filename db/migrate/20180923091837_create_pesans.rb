class CreatePesans < ActiveRecord::Migration[5.2]
  def change
    create_table :pesans do |t|
      t.string :isi
      t.references :orang, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
