class CreateCommentds < ActiveRecord::Migration[5.2]
  def change
    create_table :commentds do |t|
      t.string :commenter
      t.string :isi
      t.references :diskusi, foreign_key: true

      t.timestamps
    end
  end
end
