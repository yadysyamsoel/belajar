class CreateMobils < ActiveRecord::Migration[5.2]
  def change
    create_table :mobils do |t|
      t.string :nama
      t.string :keterangan
      t.references :orang, foreign_key: true

      t.timestamps
    end
  end
end
