class CreateDiskus < ActiveRecord::Migration[5.2]
  def change
    create_table :diskus do |t|
      t.text :isi
      t.date :tgl
      t.references :orang, foreign_key: true

      t.timestamps
    end
  end
end
