class CreateAntrians < ActiveRecord::Migration[5.2]
  def change
    create_table :antrians do |t|
      t.integer :nm
      t.string :keterangan
      t.date :tgl
      t.time :waktu
      t.references :orang, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
