# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_09_24_143800) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "antrians", force: :cascade do |t|
    t.integer "nm"
    t.string "keterangan"
    t.date "tgl"
    t.time "waktu"
    t.bigint "orang_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["orang_id"], name: "index_antrians_on_orang_id"
    t.index ["user_id"], name: "index_antrians_on_user_id"
  end

  create_table "balas", force: :cascade do |t|
    t.text "isi"
    t.string "org"
    t.bigint "pesan_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["pesan_id"], name: "index_balas_on_pesan_id"
  end

  create_table "bebeks", force: :cascade do |t|
    t.string "nama"
    t.string "email"
    t.string "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "bls", force: :cascade do |t|
    t.text "isi"
    t.string "org"
    t.bigint "pesan_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["pesan_id"], name: "index_bls_on_pesan_id"
  end

  create_table "commentdis", force: :cascade do |t|
    t.string "commenter"
    t.string "isi"
    t.bigint "diskusi_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["diskusi_id"], name: "index_commentdis_on_diskusi_id"
  end

  create_table "commentds", force: :cascade do |t|
    t.string "commenter"
    t.string "isi"
    t.bigint "diskusi_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["diskusi_id"], name: "index_commentds_on_diskusi_id"
  end

  create_table "diskus", force: :cascade do |t|
    t.text "isi"
    t.date "tgl"
    t.bigint "orang_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["orang_id"], name: "index_diskus_on_orang_id"
  end

  create_table "diskusis", force: :cascade do |t|
    t.text "isi"
    t.date "tgl"
    t.bigint "orangs_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["orangs_id"], name: "index_diskusis_on_orangs_id"
  end

  create_table "komentars", force: :cascade do |t|
    t.string "commet"
    t.string "isi"
    t.bigint "disku_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "orang_id"
    t.index ["disku_id"], name: "index_komentars_on_disku_id"
    t.index ["orang_id"], name: "index_komentars_on_orang_id"
  end

  create_table "mobils", force: :cascade do |t|
    t.string "nama"
    t.string "keterangan"
    t.bigint "orang_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["orang_id"], name: "index_mobils_on_orang_id"
  end

  create_table "orangs", force: :cascade do |t|
    t.string "nama"
    t.string "alamat"
    t.string "jk"
    t.string "foto"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "pesans", force: :cascade do |t|
    t.string "isi"
    t.bigint "orang_id"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["orang_id"], name: "index_pesans_on_orang_id"
    t.index ["user_id"], name: "index_pesans_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "password_digest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "antrians", "orangs"
  add_foreign_key "antrians", "users"
  add_foreign_key "balas", "pesans"
  add_foreign_key "bls", "pesans"
  add_foreign_key "commentdis", "diskusis"
  add_foreign_key "commentds", "diskusis"
  add_foreign_key "diskus", "orangs"
  add_foreign_key "diskusis", "orangs", column: "orangs_id"
  add_foreign_key "komentars", "diskus"
  add_foreign_key "komentars", "orangs"
  add_foreign_key "mobils", "orangs"
  add_foreign_key "pesans", "orangs"
  add_foreign_key "pesans", "users"
end
