class DiskusisController < ApplicationController
  before_action :set_diskusi, only: [:show, :edit, :update, :destroy]

  # GET /diskusis
  # GET /diskusis.json
  def index
    @diskusis = Diskusi.all
  end

  # GET /diskusis/1
  # GET /diskusis/1.json
  def show
  end

  # GET /diskusis/new
  def new
    @diskusi = Diskusi.new
  end

  # GET /diskusis/1/edit
  def edit
  end

  # POST /diskusis
  # POST /diskusis.json
  def create
    @diskusi = Diskusi.new(diskusi_params)

    respond_to do |format|
      if @diskusi.save
        format.html { redirect_to @diskusi, notice: 'Diskusi was successfully created.' }
        format.json { render :show, status: :created, location: @diskusi }
      else
        format.html { render :new }
        format.json { render json: @diskusi.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /diskusis/1
  # PATCH/PUT /diskusis/1.json
  def update
    respond_to do |format|
      if @diskusi.update(diskusi_params)
        format.html { redirect_to @diskusi, notice: 'Diskusi was successfully updated.' }
        format.json { render :show, status: :ok, location: @diskusi }
      else
        format.html { render :edit }
        format.json { render json: @diskusi.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /diskusis/1
  # DELETE /diskusis/1.json
  def destroy
    @diskusi.destroy
    respond_to do |format|
      format.html { redirect_to diskusis_url, notice: 'Diskusi was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_diskusi
      @diskusi = Diskusi.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def diskusi_params
      params.require(:diskusi).permit(:isi, :tgl, :orangs_id)
    end
end
