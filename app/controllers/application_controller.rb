class ApplicationController < ActionController::Base
 protect_from_forgery with: :exception

  # ------ add these lines here: -----

  # Make the current_user method available to views also, not just controllers:
  helper_method :current_practice
  
  # Define the current_user method:

  def current_practice
    # Look up the current user based on user_id in the session cookie:
    @current_practice ||= Bebek.find(session[:bebek_id]) if session[:bebek_id]
 
  end

  # ----- end of added lines -----
   # ----- add these lines here: -----

    # authroize method redirects user to login page if not logged in:
    def authorize
      redirect_to lbebek_path, alert: 'You must be logged in to access this page.' if current_user.nil?
    end
    
  # ----- end of added lines -----

end
