class PesansController < ApplicationController
  before_action :set_pesan, only: [:show, :edit, :update, :destroy]

  # GET /pesans
  # GET /pesans.json
  def index
    @pesans = Pesan.all
  end

  # GET /pesans/1
  # GET /pesans/1.json
  def show
  end

  # GET /pesans/new
  def new
    @pesan = Pesan.new
  end

  # GET /pesans/1/edit
  def edit
  end

  # POST /pesans
  # POST /pesans.json
  def create
    @pesan = Pesan.new(pesan_params)

    respond_to do |format|
      if @pesan.save
        format.html { redirect_to @pesan, notice: 'Pesan was successfully created.' }
        format.json { render :show, status: :created, location: @pesan }
      else
        format.html { render :new }
        format.json { render json: @pesan.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pesans/1
  # PATCH/PUT /pesans/1.json
  def update
    respond_to do |format|
      if @pesan.update(pesan_params)
        format.html { redirect_to @pesan, notice: 'Pesan was successfully updated.' }
        format.json { render :show, status: :ok, location: @pesan }
      else
        format.html { render :edit }
        format.json { render json: @pesan.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pesans/1
  # DELETE /pesans/1.json
  def destroy
    @pesan.destroy
    respond_to do |format|
      format.html { redirect_to pesans_url, notice: 'Pesan was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pesan
      @pesan = Pesan.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def pesan_params
      params.require(:pesan).permit(:isi, :orang_id, :user_id)
    end
end
