class BlsController < ApplicationController
def create
    @pesan = Pesan.find(params[:pesan_id])
    @bl = @pesan.bls.create(comment_params)
    redirect_to pesan_path(@pesan)
  end
 
  private
    def comment_params
      params.require(:bl).permit(:org, :isi)
    end
end
