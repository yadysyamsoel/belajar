class KomentarsController < ApplicationController
def create
    @disku = Disku.find(params[:disku_id])
    @komentar = @disku.komentars.create(comment_params)
    redirect_to disku_path(@disku)
  end
 
  private
    def comment_params
      params.require(:komentar).permit(:commet, :isi)
    end
end
