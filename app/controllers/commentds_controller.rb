class CommentdsController < ApplicationController
def create
    @diskusi = Diskusi.find(params[:diskusi_id])
    @commentd = @diskusi.commentds.create(comment_params)
    redirect_to article_path(@article)
  end
 
  private
    def comment_params
      params.require(:commentd).permit(:commenter, :isi)
    end
end
