class OrangsController < ApplicationController
  before_action :set_orang, only: [:show, :edit, :update, :destroy]

  # GET /orangs
  # GET /orangs.json
  def index
    @orangs = Orang.all
  end

  # GET /orangs/1
  # GET /orangs/1.json
  def show
  end

  # GET /orangs/new
  def new
    @orang = Orang.new
  end

  # GET /orangs/1/edit
  def edit
  end

  # POST /orangs
  # POST /orangs.json
  def create
    @orang = Orang.new(orang_params)

    respond_to do |format|
      if @orang.save
        format.html { redirect_to @orang, notice: 'Orang was successfully created.' }
        format.json { render :show, status: :created, location: @orang }
      else
        format.html { render :new }
        format.json { render json: @orang.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orangs/1
  # PATCH/PUT /orangs/1.json
  def update
    respond_to do |format|
      if @orang.update(orang_params)
        format.html { redirect_to @orang, notice: 'Orang was successfully updated.' }
        format.json { render :show, status: :ok, location: @orang }
      else
        format.html { render :edit }
        format.json { render json: @orang.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orangs/1
  # DELETE /orangs/1.json
  def destroy
    @orang.destroy
    respond_to do |format|
      format.html { redirect_to orangs_url, notice: 'Orang was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_orang
      @orang = Orang.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def orang_params
      params.require(:orang).permit(:nama, :alamat, :jk, :foto ,:image)
    end
end
