class DiskusController < ApplicationController
  before_action :set_disku, only: [:show, :edit, :update, :destroy]

  # GET /diskus
  # GET /diskus.json
  def index
    @diskus = Disku.all
  end

  # GET /diskus/1
  # GET /diskus/1.json
  def show
  end

  # GET /diskus/new
  def new
    @disku = Disku.new
  end

  # GET /diskus/1/edit
  def edit
  end

  # POST /diskus
  # POST /diskus.json
  def create
    @disku = Disku.new(disku_params)

    respond_to do |format|
      if @disku.save
        format.html { redirect_to @disku, notice: 'Disku was successfully created.' }
        format.json { render :show, status: :created, location: @disku }
      else
        format.html { render :new }
        format.json { render json: @disku.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /diskus/1
  # PATCH/PUT /diskus/1.json
  def update
    respond_to do |format|
      if @disku.update(disku_params)
        format.html { redirect_to @disku, notice: 'Disku was successfully updated.' }
        format.json { render :show, status: :ok, location: @disku }
      else
        format.html { render :edit }
        format.json { render json: @disku.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /diskus/1
  # DELETE /diskus/1.json
  def destroy
    @disku.destroy
    respond_to do |format|
      format.html { redirect_to diskus_url, notice: 'Disku was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_disku
      @disku = Disku.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def disku_params
      params.require(:disku).permit(:isi, :tgl, :orang_id)
    end
end
