class AntriansController < ApplicationController
  before_action :set_antrian, only: [:show, :edit, :update, :destroy]

  # GET /antrians
  # GET /antrians.json
  def index
    d = DateTime.now
    proses = "Proses"
    menunggu = "Menunggu"
    @antrians = Antrian.where(tgl: d).order(waktu: :asc)
    
    @hitung = @antrians.count

    @an = @antrians.where(keterangan: proses)
    @ht = @an.count

    @ant =  @antrians.where(keterangan: menunggu)   
    @mn = @ant.count
    
     @idku = current_user.id
     #@aku = Antrian.select(:nm).where(tgl: d , orang_id: @idku)

    # idku = current_user.id

    #@aku = @antrians.find(@idku)

    #@aku = Antrian.where(orang_id: @idku)
    # @aku = Antrian.where(orang_id: @idku , tgl: d)
  end

  # GET /antrians/1
  # GET /antrians/1.json
  def show
  end

  # GET /antrians/new
  def new
    m = DateTime.now
    @antrian = Antrian.new
    @antriuns = Antrian.where(tgl: m).order(waktu: :asc)
    @ss = @antriuns.count

  end

  # GET /antrians/1/edit
  def edit
  end

  # POST /antrians
  # POST /antrians.json
  def create
    @antrian = Antrian.new(antrian_params)

    respond_to do |format|
      if @antrian.save
        format.html { redirect_to @antrian, notice: 'Antrian was successfully created.' }
        format.json { render :show, status: :created, location: @antrian }
      else
        format.html { render :new }
        format.json { render json: @antrian.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /antrians/1
  # PATCH/PUT /antrians/1.json
  def update
    respond_to do |format|
      if @antrian.update(antrian_params)
        format.html { redirect_to @antrian, notice: 'Antrian was successfully updated.' }
        format.json { render :show, status: :ok, location: @antrian }
      else
        format.html { render :edit }
        format.json { render json: @antrian.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /antrians/1
  # DELETE /antrians/1.json
  def destroy
    @antrian.destroy
    respond_to do |format|
      format.html { redirect_to antrians_url, notice: 'Antrian was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_antrian
      @antrian = Antrian.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def antrian_params
      params.require(:antrian).permit(:nm, :keterangan, :tgl, :waktu, :orang_id, :user_id)
    end
end
