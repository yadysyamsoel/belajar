class Orang < ApplicationRecord
# has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
#   validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
has_attached_file :image, :styles => { :small => "150x150>" },
                  :url  => "/assets/orangs/:id/:style/:basename.:extension",
                  :path => ":rails_root/public/assets/orangs/:id/:style/:basename.:extension"

validates_attachment_presence :image
validates_attachment_size :image, :less_than => 5.megabytes
validates_attachment_content_type :image, :content_type => ['image/jpeg', 'image/png']
end
