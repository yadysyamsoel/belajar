Rails.application.routes.draw do
  resources :antrians
  resources :pesans do
  	resources :bls
  end
  resources :diskus do
  	resources :komentars
  end

  resources :diskusis do
  	resources :commentds
  end
  resources :mobils
  resources :orangs
  get 'welcome/index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  root 'orangs#index'

  # ----- add these lines here: -----

	# Add a root route if you don't have one...
	# We can use users#new for now, or replace this with the controller and action you want to be the site root:
	root to: 'users#new'
  	
  	# sign up page with form:
	get 'users/new' => 'users#new', as: :new_user
	
	# create (post) action for when sign up form is submitted:
	post 'users' => 'users#create'

  	# ----- end of added lines -----

  	# ----- add these lines here: -----
	
	# log in page with form:
	get '/login'     => 'sessions#new'
	
	# create (post) action for when log in form is submitted:
	post '/login'    => 'sessions#create'
	
	# delete action to log out:
	delete '/logout' => 'sessions#destroy'  
  	
  	# ----- end of added lines -----

  # ----- add these lines here: -----

  # Add a root route if you don't have one...
  # We can use users#new for now, or replace this with the controller and action you want to be the site root:
  #root to: 'users#new'
    
    # sign up page with form:
  get 'bebeks/new' => 'bebeks#new', as: :new_bebek
  
  # create (post) action for when sign up form is submitted:
  post 'bebeks' => 'bebeks#create'

    # ----- end of added lines -----
    # ----- add these lines here: -----
  
  # log in page with form:
  get '/lbebek'     => 'lbebeks#new'
  
  # create (post) action for when log in form is submitted:
  post '/lbebek'    => 'lbebeks#create'
  
  # delete action to log out:
  delete '/logout' => 'lbebeks#destroy'  
    
    # ----- end of added lines -----


end
