require 'test_helper'

class OrangsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @orang = orangs(:one)
  end

  test "should get index" do
    get orangs_url
    assert_response :success
  end

  test "should get new" do
    get new_orang_url
    assert_response :success
  end

  test "should create orang" do
    assert_difference('Orang.count') do
      post orangs_url, params: { orang: { alamat: @orang.alamat, foto: @orang.foto, jk: @orang.jk, nama: @orang.nama } }
    end

    assert_redirected_to orang_url(Orang.last)
  end

  test "should show orang" do
    get orang_url(@orang)
    assert_response :success
  end

  test "should get edit" do
    get edit_orang_url(@orang)
    assert_response :success
  end

  test "should update orang" do
    patch orang_url(@orang), params: { orang: { alamat: @orang.alamat, foto: @orang.foto, jk: @orang.jk, nama: @orang.nama } }
    assert_redirected_to orang_url(@orang)
  end

  test "should destroy orang" do
    assert_difference('Orang.count', -1) do
      delete orang_url(@orang)
    end

    assert_redirected_to orangs_url
  end
end
