require 'test_helper'

class PesansControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pesan = pesans(:one)
  end

  test "should get index" do
    get pesans_url
    assert_response :success
  end

  test "should get new" do
    get new_pesan_url
    assert_response :success
  end

  test "should create pesan" do
    assert_difference('Pesan.count') do
      post pesans_url, params: { pesan: { isi: @pesan.isi, orang_id: @pesan.orang_id, user_id: @pesan.user_id } }
    end

    assert_redirected_to pesan_url(Pesan.last)
  end

  test "should show pesan" do
    get pesan_url(@pesan)
    assert_response :success
  end

  test "should get edit" do
    get edit_pesan_url(@pesan)
    assert_response :success
  end

  test "should update pesan" do
    patch pesan_url(@pesan), params: { pesan: { isi: @pesan.isi, orang_id: @pesan.orang_id, user_id: @pesan.user_id } }
    assert_redirected_to pesan_url(@pesan)
  end

  test "should destroy pesan" do
    assert_difference('Pesan.count', -1) do
      delete pesan_url(@pesan)
    end

    assert_redirected_to pesans_url
  end
end
