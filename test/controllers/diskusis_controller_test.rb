require 'test_helper'

class DiskusisControllerTest < ActionDispatch::IntegrationTest
  setup do
    @diskusi = diskusis(:one)
  end

  test "should get index" do
    get diskusis_url
    assert_response :success
  end

  test "should get new" do
    get new_diskusi_url
    assert_response :success
  end

  test "should create diskusi" do
    assert_difference('Diskusi.count') do
      post diskusis_url, params: { diskusi: { isi: @diskusi.isi, orangs_id: @diskusi.orangs_id, tgl: @diskusi.tgl } }
    end

    assert_redirected_to diskusi_url(Diskusi.last)
  end

  test "should show diskusi" do
    get diskusi_url(@diskusi)
    assert_response :success
  end

  test "should get edit" do
    get edit_diskusi_url(@diskusi)
    assert_response :success
  end

  test "should update diskusi" do
    patch diskusi_url(@diskusi), params: { diskusi: { isi: @diskusi.isi, orangs_id: @diskusi.orangs_id, tgl: @diskusi.tgl } }
    assert_redirected_to diskusi_url(@diskusi)
  end

  test "should destroy diskusi" do
    assert_difference('Diskusi.count', -1) do
      delete diskusi_url(@diskusi)
    end

    assert_redirected_to diskusis_url
  end
end
