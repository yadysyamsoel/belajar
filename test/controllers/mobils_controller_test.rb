require 'test_helper'

class MobilsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @mobil = mobils(:one)
  end

  test "should get index" do
    get mobils_url
    assert_response :success
  end

  test "should get new" do
    get new_mobil_url
    assert_response :success
  end

  test "should create mobil" do
    assert_difference('Mobil.count') do
      post mobils_url, params: { mobil: { keterangan: @mobil.keterangan, nama: @mobil.nama, orang_id: @mobil.orang_id } }
    end

    assert_redirected_to mobil_url(Mobil.last)
  end

  test "should show mobil" do
    get mobil_url(@mobil)
    assert_response :success
  end

  test "should get edit" do
    get edit_mobil_url(@mobil)
    assert_response :success
  end

  test "should update mobil" do
    patch mobil_url(@mobil), params: { mobil: { keterangan: @mobil.keterangan, nama: @mobil.nama, orang_id: @mobil.orang_id } }
    assert_redirected_to mobil_url(@mobil)
  end

  test "should destroy mobil" do
    assert_difference('Mobil.count', -1) do
      delete mobil_url(@mobil)
    end

    assert_redirected_to mobils_url
  end
end
