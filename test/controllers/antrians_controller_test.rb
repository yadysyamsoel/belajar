require 'test_helper'

class AntriansControllerTest < ActionDispatch::IntegrationTest
  setup do
    @antrian = antrians(:one)
  end

  test "should get index" do
    get antrians_url
    assert_response :success
  end

  test "should get new" do
    get new_antrian_url
    assert_response :success
  end

  test "should create antrian" do
    assert_difference('Antrian.count') do
      post antrians_url, params: { antrian: { keterangan: @antrian.keterangan, nm: @antrian.nm, orang_id: @antrian.orang_id, tgl: @antrian.tgl, user_id: @antrian.user_id, waktu: @antrian.waktu } }
    end

    assert_redirected_to antrian_url(Antrian.last)
  end

  test "should show antrian" do
    get antrian_url(@antrian)
    assert_response :success
  end

  test "should get edit" do
    get edit_antrian_url(@antrian)
    assert_response :success
  end

  test "should update antrian" do
    patch antrian_url(@antrian), params: { antrian: { keterangan: @antrian.keterangan, nm: @antrian.nm, orang_id: @antrian.orang_id, tgl: @antrian.tgl, user_id: @antrian.user_id, waktu: @antrian.waktu } }
    assert_redirected_to antrian_url(@antrian)
  end

  test "should destroy antrian" do
    assert_difference('Antrian.count', -1) do
      delete antrian_url(@antrian)
    end

    assert_redirected_to antrians_url
  end
end
