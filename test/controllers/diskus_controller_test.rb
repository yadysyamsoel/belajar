require 'test_helper'

class DiskusControllerTest < ActionDispatch::IntegrationTest
  setup do
    @disku = diskus(:one)
  end

  test "should get index" do
    get diskus_url
    assert_response :success
  end

  test "should get new" do
    get new_disku_url
    assert_response :success
  end

  test "should create disku" do
    assert_difference('Disku.count') do
      post diskus_url, params: { disku: { isi: @disku.isi, orang_id: @disku.orang_id, tgl: @disku.tgl } }
    end

    assert_redirected_to disku_url(Disku.last)
  end

  test "should show disku" do
    get disku_url(@disku)
    assert_response :success
  end

  test "should get edit" do
    get edit_disku_url(@disku)
    assert_response :success
  end

  test "should update disku" do
    patch disku_url(@disku), params: { disku: { isi: @disku.isi, orang_id: @disku.orang_id, tgl: @disku.tgl } }
    assert_redirected_to disku_url(@disku)
  end

  test "should destroy disku" do
    assert_difference('Disku.count', -1) do
      delete disku_url(@disku)
    end

    assert_redirected_to diskus_url
  end
end
