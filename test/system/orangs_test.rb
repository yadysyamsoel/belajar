require "application_system_test_case"

class OrangsTest < ApplicationSystemTestCase
  setup do
    @orang = orangs(:one)
  end

  test "visiting the index" do
    visit orangs_url
    assert_selector "h1", text: "Orangs"
  end

  test "creating a Orang" do
    visit orangs_url
    click_on "New Orang"

    fill_in "Alamat", with: @orang.alamat
    fill_in "Foto", with: @orang.foto
    fill_in "Jk", with: @orang.jk
    fill_in "Nama", with: @orang.nama
    click_on "Create Orang"

    assert_text "Orang was successfully created"
    click_on "Back"
  end

  test "updating a Orang" do
    visit orangs_url
    click_on "Edit", match: :first

    fill_in "Alamat", with: @orang.alamat
    fill_in "Foto", with: @orang.foto
    fill_in "Jk", with: @orang.jk
    fill_in "Nama", with: @orang.nama
    click_on "Update Orang"

    assert_text "Orang was successfully updated"
    click_on "Back"
  end

  test "destroying a Orang" do
    visit orangs_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Orang was successfully destroyed"
  end
end
