require "application_system_test_case"

class PesansTest < ApplicationSystemTestCase
  setup do
    @pesan = pesans(:one)
  end

  test "visiting the index" do
    visit pesans_url
    assert_selector "h1", text: "Pesans"
  end

  test "creating a Pesan" do
    visit pesans_url
    click_on "New Pesan"

    fill_in "Isi", with: @pesan.isi
    fill_in "Orang", with: @pesan.orang_id
    fill_in "User", with: @pesan.user_id
    click_on "Create Pesan"

    assert_text "Pesan was successfully created"
    click_on "Back"
  end

  test "updating a Pesan" do
    visit pesans_url
    click_on "Edit", match: :first

    fill_in "Isi", with: @pesan.isi
    fill_in "Orang", with: @pesan.orang_id
    fill_in "User", with: @pesan.user_id
    click_on "Update Pesan"

    assert_text "Pesan was successfully updated"
    click_on "Back"
  end

  test "destroying a Pesan" do
    visit pesans_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Pesan was successfully destroyed"
  end
end
