require "application_system_test_case"

class MobilsTest < ApplicationSystemTestCase
  setup do
    @mobil = mobils(:one)
  end

  test "visiting the index" do
    visit mobils_url
    assert_selector "h1", text: "Mobils"
  end

  test "creating a Mobil" do
    visit mobils_url
    click_on "New Mobil"

    fill_in "Keterangan", with: @mobil.keterangan
    fill_in "Nama", with: @mobil.nama
    fill_in "Orang", with: @mobil.orang_id
    click_on "Create Mobil"

    assert_text "Mobil was successfully created"
    click_on "Back"
  end

  test "updating a Mobil" do
    visit mobils_url
    click_on "Edit", match: :first

    fill_in "Keterangan", with: @mobil.keterangan
    fill_in "Nama", with: @mobil.nama
    fill_in "Orang", with: @mobil.orang_id
    click_on "Update Mobil"

    assert_text "Mobil was successfully updated"
    click_on "Back"
  end

  test "destroying a Mobil" do
    visit mobils_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Mobil was successfully destroyed"
  end
end
