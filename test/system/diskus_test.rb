require "application_system_test_case"

class DiskusTest < ApplicationSystemTestCase
  setup do
    @disku = diskus(:one)
  end

  test "visiting the index" do
    visit diskus_url
    assert_selector "h1", text: "Diskus"
  end

  test "creating a Disku" do
    visit diskus_url
    click_on "New Disku"

    fill_in "Isi", with: @disku.isi
    fill_in "Orang", with: @disku.orang_id
    fill_in "Tgl", with: @disku.tgl
    click_on "Create Disku"

    assert_text "Disku was successfully created"
    click_on "Back"
  end

  test "updating a Disku" do
    visit diskus_url
    click_on "Edit", match: :first

    fill_in "Isi", with: @disku.isi
    fill_in "Orang", with: @disku.orang_id
    fill_in "Tgl", with: @disku.tgl
    click_on "Update Disku"

    assert_text "Disku was successfully updated"
    click_on "Back"
  end

  test "destroying a Disku" do
    visit diskus_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Disku was successfully destroyed"
  end
end
