require "application_system_test_case"

class AntriansTest < ApplicationSystemTestCase
  setup do
    @antrian = antrians(:one)
  end

  test "visiting the index" do
    visit antrians_url
    assert_selector "h1", text: "Antrians"
  end

  test "creating a Antrian" do
    visit antrians_url
    click_on "New Antrian"

    fill_in "Keterangan", with: @antrian.keterangan
    fill_in "Nm", with: @antrian.nm
    fill_in "Orang", with: @antrian.orang_id
    fill_in "Tgl", with: @antrian.tgl
    fill_in "User", with: @antrian.user_id
    fill_in "Waktu", with: @antrian.waktu
    click_on "Create Antrian"

    assert_text "Antrian was successfully created"
    click_on "Back"
  end

  test "updating a Antrian" do
    visit antrians_url
    click_on "Edit", match: :first

    fill_in "Keterangan", with: @antrian.keterangan
    fill_in "Nm", with: @antrian.nm
    fill_in "Orang", with: @antrian.orang_id
    fill_in "Tgl", with: @antrian.tgl
    fill_in "User", with: @antrian.user_id
    fill_in "Waktu", with: @antrian.waktu
    click_on "Update Antrian"

    assert_text "Antrian was successfully updated"
    click_on "Back"
  end

  test "destroying a Antrian" do
    visit antrians_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Antrian was successfully destroyed"
  end
end
