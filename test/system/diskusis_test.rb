require "application_system_test_case"

class DiskusisTest < ApplicationSystemTestCase
  setup do
    @diskusi = diskusis(:one)
  end

  test "visiting the index" do
    visit diskusis_url
    assert_selector "h1", text: "Diskusis"
  end

  test "creating a Diskusi" do
    visit diskusis_url
    click_on "New Diskusi"

    fill_in "Isi", with: @diskusi.isi
    fill_in "Orangs", with: @diskusi.orangs_id
    fill_in "Tgl", with: @diskusi.tgl
    click_on "Create Diskusi"

    assert_text "Diskusi was successfully created"
    click_on "Back"
  end

  test "updating a Diskusi" do
    visit diskusis_url
    click_on "Edit", match: :first

    fill_in "Isi", with: @diskusi.isi
    fill_in "Orangs", with: @diskusi.orangs_id
    fill_in "Tgl", with: @diskusi.tgl
    click_on "Update Diskusi"

    assert_text "Diskusi was successfully updated"
    click_on "Back"
  end

  test "destroying a Diskusi" do
    visit diskusis_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Diskusi was successfully destroyed"
  end
end
